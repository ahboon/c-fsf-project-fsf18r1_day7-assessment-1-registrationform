export class RegistrationUser {
    constructor(
        public email: string,
        public password: string,
        public name: string,
        public gender: string,
        public dateOfBirth: Date,
        public address: string,
        public country: string,
        public contactNumber: string,
        public age: number,
        
    ){

    }
}
