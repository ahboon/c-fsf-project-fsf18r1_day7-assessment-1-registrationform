import { Component } from '@angular/core';
import { RegistrationUser } from './shared/registration-user';
import { UserRegistrationService } from './services/user-registration.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  model = new RegistrationUser('','','','',null,'','','',null);
  gender: string[] = ['M', 'F'];
  
  
                  
  isSubmitted: boolean = false;
 
  result: string = "";
 
  users: any;

  

  constructor(private userService: UserRegistrationService){

  }
  
 
  
 // ngOnInit() {
 //   this.model = new RegistrationUser('','','','',null,'','','',null);
   
    
 
//  }
  
  
  onSubmit(){
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.name);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.country);
    console.log(this.model.contactNumber);
    console.log(this.model.age);
    this.userService.saveUserRegistration(this.model)
      .subscribe(users => {
        console.log('send to backend !');
        console.log(users);
        this.users = users;
      })
    
  }

}
